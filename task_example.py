input_list = [

    {
        "name": "andy",
        "second_name": "kovv",
        "date_of_birth": "22.07.1991",
        "city": "Ternopil",
        "age": 31,
    },
    {
        "name": "andy2",
        "second_name": "kovv2",
        "date_of_birth": "22.07.1993",
        "city": "Ternopil2",
        "age": 40,
    },
]
schema = {
    "user_full_name": "$schema.user_name + $schema.user_second_name",
    "user_name": "$input_list.name",
    "user_second_name": "$input_list.second_name",
    "user_age": "$input_list.age",
    "user_max_age": "$schema.user_age + 10",
    "city": "$input_list.city", #add new key city
    "date_of_birth": "$input_list.date_of_birth", #add new key birthday
}



def mapper(input_list, schema):
    new_list = []
    for item in input_list:
        new_schema_dict = {}
        for key, value in schema.items():
            if value.startswith("$input_list"):
                field_name = value.split(".")[-1]
                new_schema_dict[key] = item[field_name]
            elif value.startswith("$schema"):
                parts = [part.strip() for part in value.split("+")]
                field_name_first_part = parts[0].split(".")[-1]
                field_name_second_part = parts[1].split(".")[-1]
                if field_name_second_part.isdigit(): # Содержит только цифры
                    field_values = new_schema_dict[field_name_first_part] + eval(field_name_second_part)
                    new_schema_dict[key] = field_values
                else:
                    field_value = " ".join([new_schema_dict[field_name_first_part], new_schema_dict[field_name_second_part]])
                    new_schema_dict[key] = field_value

        new_list.append(new_schema_dict)
    return print(new_list)


result = mapper(input_list, schema)
result == [
    {
        "user_full_name": "andy kovv",
        "user_name": "andy",
        "user_second_name": "kovv",
        "user_age": "31",
        "user_max_age": "41",
        "city" : "Ternopil", # new key, value
        "date_of_birth" : "22.07.1991", # new key, value

    },
    {
        "user_full_name": "andy2 kovv2",
        "user_name": "andy2",
        "user_second_name": "kovv2",
        "user_age": "40",
        "user_max_age": "50",
        "city" : "Ternopil2", # new key, value
        "date_of_birth" : "22.07.1993", # new key, value
    },
]